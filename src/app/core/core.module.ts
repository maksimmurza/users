import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserListComponent } from './user-list/user-list.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserComponent } from './user/user.component';
import {UserListService} from '../services/user-list.service/user-list.service';
import {HttpClientModule} from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    NgbModule.forRoot()
  ],
  providers: [
    UserListService,
  ],
  declarations: [
    UserListComponent,
    UserDetailComponent,
    UserComponent
  ],
  exports: [
    UserListComponent,
    UserDetailComponent,
    UserComponent
  ],
  entryComponents: [
    UserDetailComponent
  ]
})
export class CoreModule { }
