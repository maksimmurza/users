import {Component, OnInit, ViewChild} from '@angular/core';
import {UserListService} from '../../services/user-list.service/user-list.service';
import {User} from '../../models/user.model';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {UserDetailComponent} from '../user-detail/user-detail.component';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  public users: User[];
  public modalRef: any;

  constructor(private userListService: UserListService,
              private modalService: NgbModal) { }

  ngOnInit() {
    this.getUsers();
  }

  getUsers(): void {
    this.userListService.requestUsers()
      .subscribe(users => {
        this.users = users;
        if (this.userSaved()) {
          this.open();
        }
      });
  }

  getUserById(id: number): User {
    let user: User;
    for (user of this.users) {
      if (user.id === id) {
        return user;
      }
    }
  }

  selectUser(user: User): void {
    this.userListService.setSelectedUser(user);
    localStorage.setItem('selectedUserId', user.id.toString());
  }

  getSelectedUser(): User {
    return this.userListService.getSelectedUser();
  }

  open() {
    this.modalRef = this.modalService.open(UserDetailComponent);
    this.modalRef.componentInstance.user = this.getSelectedUser();
    this.modalRef.componentInstance.activeModalRef = this.modalRef;
  }

  public userSaved(): boolean {
    if (localStorage.getItem('selectedUserId')) {
      this.selectUser(this.getUserById(Number(localStorage.getItem('selectedUserId'))));
      return true;
    } else {
      return false;
    }
  }
}
