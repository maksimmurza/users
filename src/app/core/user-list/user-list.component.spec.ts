import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserListComponent } from './user-list.component';
import { UserListService } from '../../services/user-list.service/user-list.service';
import {User} from '../../models/user.model';
import {expectedUsers} from '../../models/users-test';
import { UserComponent } from '../user/user.component';
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { NgModule } from '@angular/core';
import { UserDetailComponent } from '../user-detail/user-detail.component';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';


// Module that is imported in TestBed, where is impossible
// to describe entry components

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    NgbModule.forRoot(),
  ],
  declarations: [
    UserListComponent,
    UserDetailComponent,
    UserComponent
  ],
  entryComponents: [
    UserDetailComponent
  ]
})
export class TestingModule { }

describe('UserListComponent', () => {
  const userId = 1;
  let component: UserListComponent;
  let fixture: ComponentFixture<UserListComponent>;
  let userListService: jasmine.SpyObj<UserListService>;

  beforeEach(async(() => {
    const spy = jasmine.createSpyObj('UserListService', [
      'requestUsers',
      'setSelectedUser',
      'getSelectedUser']);

    localStorage.removeItem('selectedUserId');

    TestBed.configureTestingModule({
      imports: [TestingModule],
      providers: [{ provide: UserListService, useValue: spy },
        NgbActiveModal]
    });

    userListService = TestBed.get(UserListService);
    userListService.requestUsers.and.returnValue(Observable.of(expectedUsers));
    userListService.getSelectedUser.and.returnValue(expectedUsers[userId - 1]);
    fixture = TestBed.createComponent(UserListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get users from server', () => {
    component.getUsers();
    expect(component.users).toBe(expectedUsers);
  });

  it('should get user by id', () => {
    let returnedUser: User;
    component.users = expectedUsers;
    returnedUser = component.getUserById(userId);
    expect(returnedUser).toBe(expectedUsers[userId - 1]);
  });

  it('should select user (write object in service field and local storage)', () => {
   component.selectUser(expectedUsers[userId - 1]);
   expect(userListService.getSelectedUser()).toBe(expectedUsers[userId - 1]);
   expect(localStorage.getItem('selectedUserId')).toBe(userId.toString());
  });

  it('should get selected user', () => {
    const selectedUser = component.getSelectedUser();
    expect(selectedUser).toBe(expectedUsers[userId - 1]);
  });

  it('should open modal window', () => {
    component.selectUser(expectedUsers[userId - 1]);
    component.open();
    expect(component.modalRef.componentInstance.user).toBe(expectedUsers[userId - 1]);
    expect(component.modalRef.componentInstance.activeModalRef).toBe(component.modalRef);
  });

  it('should check if user is saved in local storage', () => {
   expect(component.userSaved()).toBe(false);
   localStorage.setItem('selectedUserId', '1');
   expect(component.userSaved()).toBe(true);
  });
});
