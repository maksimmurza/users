import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDetailComponent } from './user-detail.component';
import { UserListService } from '../../services/user-list.service/user-list.service';
import {expectedUsers} from '../../models/users-test';
import { User } from '../../models/user.model';

describe('UserDetailComponent', () => {
  let component: UserDetailComponent;
  let fixture: ComponentFixture<UserDetailComponent>;
  let userListService: jasmine.SpyObj<UserListService>;

  beforeEach(async(() => {
    const spy = jasmine.createSpyObj('UserListService', [
      'setSelectedUser',
      'getSelectedUser']);

    TestBed.configureTestingModule({
      declarations: [ UserDetailComponent ],
      providers: [{ provide: UserListService, useValue: spy }]
    });

    userListService = TestBed.get(UserListService);
    fixture = TestBed.createComponent(UserDetailComponent);
    component = fixture.componentInstance;
    component.user = expectedUsers[0];
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should deselect user when modal window is closing', () => {
    localStorage.setItem('selectedUserId', component.user.id.toString());

    userListService.setSelectedUser.and.callFake((value) => {
      if (value == null) {
        userListService.getSelectedUser.and.returnValue(null);
      }
    });

    component.deselectUser();

    expect(localStorage.getItem('selectedUserId')).toBeNull();
    expect(userListService.getSelectedUser()).toBeNull();
  });

  it('should toggle address section', () => {
    const addressState = component.addressState;
    component.toggleAddressState();
    expect(addressState).not.toBe(component.addressState);
  });
});
