import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../models/user.model';
import {UserListService} from '../../services/user-list.service/user-list.service';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

@Component({
  providers: [NgbActiveModal],
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  @Input() user: User;
  @Input() activeModalRef: NgbModalRef;
  public addressState = false;

  constructor(private userListService: UserListService,
              public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  deselectUser() {
    this.userListService.setSelectedUser(null);
    localStorage.removeItem('selectedUserId');
  }

  toggleAddressState() {
    this.addressState = !this.addressState;
  }
}
