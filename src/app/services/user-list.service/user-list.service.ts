import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../../models/user.model';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class UserListService {

  private url = 'https://jsonplaceholder.typicode.com/users';
  private selectedUser: User;

  constructor(private http: HttpClient) {}

  requestUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.url);
  }

  setSelectedUser(user: User): void {
    this.selectedUser = user;
  }

  getSelectedUser(): User {
    return this.selectedUser;
  }

  setUrl(url: string) {
    this.url = url;
  }

  getUrl() {
    return this.url;
  }

}
