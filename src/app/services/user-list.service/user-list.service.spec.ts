import { TestBed, inject } from '@angular/core/testing';

import { UserListService } from './user-list.service';
import {User} from '../../models/user.model';
import {expectedUsers} from '../../models/users-test';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('UserListService', () => {
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserListService]
    });

    httpMock = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', inject([UserListService],
    (service: UserListService) => {
      expect(service).toBeTruthy();
    }));

  it('should return right quantity of objects and obj. values',
    inject([UserListService],
    (service: UserListService) => {

      let receivedUsers: User[];

      // Compare returned data values with expected
      // to check receive handling

      service.requestUsers().subscribe(users => {
        receivedUsers = users;
        expect(receivedUsers.length).toBe(expectedUsers.length);
        expect(receivedUsers[0].name).toEqual(expectedUsers[0].name);
      });

      // Expect request to url with specific http method
      // Return mock data if request is detected

      const req = httpMock.expectOne(service.getUrl());
      expect(req.request.method).toBe('GET');
      req.flush(expectedUsers);
    }));
});
