# Users

After downloading the project install dependencies:  
```
$ cd users  
$ npm install
```

## Development server

Next command will run dev server:  
`$ npm start`  
The app will automatically reload if you change any of the source files.


## Running unit tests and code check

`$ npm test` - running unit tests via [Karma](https://karma-runner.github.io).  
`$ npm lint` - running tslint to check you code.

## Build

Next command builds the project straight to the production build:  
`$ npm build`  
The build artifacts will be stored in the `dist/` directory.

## Basic web-server

* Install express:  
`$ npm install -g express`

* Create `server.js` file in parent folder of project directory (otherwise use another path to dist folder in code) with next code:

```js
const express = require('express');
const path = require('path');
const port = process.env.PORT || 8080;
const app = express();

app.use(express.static('./users' + '/dist'));

app.get('*', function (request, response) {
  response.sendFile(path.resolve(__dirname, 'index.html'));
});

app.listen(port);
console.log("server started on port " + port);
```
* Run server in you terminal:  
`$ node server.js`  

* Open application in browser following the link from message in terminal